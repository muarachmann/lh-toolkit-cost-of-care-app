import 'package:curativecare/screens/saved/components/saved_list.dart';
import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SavedList(),
    );
  }
}
